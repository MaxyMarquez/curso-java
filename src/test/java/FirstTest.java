import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by alejandroCragnolini on 4/17/14.
 */
public class FirstTest {

    private First instance;

    @Before
    public void setUp() {
        instance = new First();
    }

    @Test
    public void testIsZero() {
        Assert.assertTrue(instance.isZero(0));
        Assert.assertFalse(instance.isZero(1));
    }
}
